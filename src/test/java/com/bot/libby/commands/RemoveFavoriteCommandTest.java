package com.bot.libby.commands;

import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RemoveFavoriteCommandTest {
    AddFavoriteCommand addFavoriteCommand;
    RemoveFavoriteCommand removeFavoriteCommand;
    RegisteredUsers registeredUsers = RegisteredUsers.getInstance();

    @Before
    public void setUp(){
        registeredUsers.addRegisteredUser(new User("Rafif Taris","123123"));
        addFavoriteCommand = new AddFavoriteCommand();
        removeFavoriteCommand = new RemoveFavoriteCommand();
    }

    @Test
    public void testSetGetQuery(){
        String[] query = {"/fav", "92438512126123"};
        removeFavoriteCommand.setQuery(query);
        assertEquals(query,removeFavoriteCommand.getQuery());
    }

    @Test
    public void testExecute(){
        String[] query = {"/fav", "9786027870994","123123"};
        addFavoriteCommand.setQuery(query);
        removeFavoriteCommand.setQuery(query);
        addFavoriteCommand.execute();
        removeFavoriteCommand.execute();
        assertTrue(removeFavoriteCommand.success);
    }
}
