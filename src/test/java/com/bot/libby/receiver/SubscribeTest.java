package com.bot.libby.receiver;

import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SubscribeTest {
    private Subscribe subscribe;
    private User user;
    private RegisteredUsers registeredUsers;

    @Before
    public void setUp() {
        user = new User("John", "user1");
        registeredUsers = RegisteredUsers.getInstance();
        registeredUsers.addRegisteredUser(user);
        subscribe = new Subscribe();
    }

    @Test
    public void addAuthorTest() {
        subscribe.addAuthor("James", "user1");
        assertEquals(user.getAuthorList().getList().size(), 1);
    }
}