package com.bot.libby.receiver;

import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class FavoriteTest {
    Favorite favorite;
    User user;
    RegisteredUsers registeredUsers;

    @Before
    public void setUp(){
        user = new User("Rafif", "15126");
        registeredUsers = RegisteredUsers.getInstance();
        registeredUsers.addRegisteredUser(user);
        favorite = new Favorite();

    }

    @Test
    public void testAddFavoriteSuccess(){
        assertTrue(favorite.addBook("9790660871","15126"));
    }

    @Test
    public void testAddFavoriteFail(){
        favorite.addBook("9786027870994","15126");
        assertFalse(favorite.addBook("9786027870994","15126"));
    }

    @Test
    public void testRemoveFavoriteSuccess(){
        favorite.addBook("9790660871","15126");
        assertTrue(favorite.removeBook("9790660871","15126"));
    }

    @Test
    public void testRemoveFavoriteFail(){
        favorite.addBook("9786027870994","15126");
        favorite.removeBook("9786027870994","15126");
        assertFalse(favorite.removeBook("9786027870994","15126"));
    }
}
