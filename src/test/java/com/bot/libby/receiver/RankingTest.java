  package com.bot.libby.receiver;

  import com.bot.libby.resources.Book;
  import com.bot.libby.resources.RegisteredUsers;
  import org.junit.Test;
  import org.junit.runner.RunWith;
  import org.mockito.junit.MockitoJUnitRunner;
  import org.springframework.boot.test.context.SpringBootTest;

  import java.util.ArrayList;
  import java.util.List;

  import static junit.framework.TestCase.assertEquals;
  import static junit.framework.TestCase.assertTrue;

  @RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RankingTest {
    Ranking ranking = Ranking.getInstance();
    RegisteredUsers registeredUsers = ranking.getRegisteredUsers();
    ApiRequestBook apiRequestBook = ApiRequestBook.getInstance();

    @Test
    public void testUpdateBookRanking(){
        String[] query = {"/search", "isbn", "9786027870994"};
        String[] query2 = {"/search", "isbn", "9790660871"};

        //users adding book
        ranking.updateRanking(apiRequestBook.generateQuery(query2).get(0),"add");
        ranking.updateRanking(apiRequestBook.generateQuery(query).get(0),"add");
        ranking.updateRanking(apiRequestBook.generateQuery(query).get(0),"add");

        //comparing list
        List<Book> bookList = new ArrayList<>();
        bookList.add(apiRequestBook.generateQuery(query).get(0));
        bookList.add(apiRequestBook.generateQuery(query2).get(0));
        assertTrue(bookList.get(0).getIsbn().equals(ranking.showBookRanking().get(0).getIsbn()));
        assertTrue(bookList.get(1).getIsbn().equals(ranking.showBookRanking().get(1).getIsbn()));

        //Delete when count <= 1
        ranking.updateRanking(apiRequestBook.generateQuery(query2).get(0),"delete");
        assertEquals(1,ranking.showBookRanking().size());

        //Delete when count > 1
        ranking.updateRanking(apiRequestBook.generateQuery(query).get(0),"delete");
        assertTrue(ranking.showBookRanking().size() == 1);
    }

    @Test
    public void testUpdateAuthorRanking(){
        String author = "Tere Liye";
        String author2 = "Dee Lestari";

        //add author to the ranking
        ranking.updateRanking(author,"add");
        ranking.updateRanking(author2,"add");
        ranking.updateRanking(author,"add");
        ranking.updateRanking("Peter", "delete");

        //List to compare
        List<String> authors = new ArrayList<>();
        authors.add(author);
        authors.add(author2);
        assertEquals(authors,ranking.showAuthorRanking());

        //Delete when count <= 1
        ranking.updateRanking(author2,"delete");
        assertTrue(ranking.showAuthorRanking().size()==1);

        //Delete when count > 1
        ranking.updateRanking(author,"delete");
        assertTrue(ranking.showAuthorRanking().size()==1);
    }
}
