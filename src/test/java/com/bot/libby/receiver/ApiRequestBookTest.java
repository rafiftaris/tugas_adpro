package com.bot.libby.receiver;

import com.bot.libby.resources.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ApiRequestBookTest {
    @InjectMocks
    private ApiRequestBook apiRequestBook;

    @InjectMocks
    private Book book;

    @Test
    public void queryTestIsbnSuccess(){
        String[] query = {"/search", "isbn", "9786027870994"};
        assertTrue("Result is not null", apiRequestBook.generateQuery(query) != null);
    }

    @Test
    public void queryTestSubjectSuccess() throws NoSuchFieldException, IllegalAccessException {
        String[] query = {"/search", "genre", "Dilan"};
        assertTrue("Result is not null", apiRequestBook.generateQuery(query) != null);
    }

    @Test
    public void queryTestAuthorFails() throws NoSuchFieldException, IllegalAccessException {
        String[] query = {"/search", "author", "asd", "asd", "asd"};
        assertTrue("Result is empty list", apiRequestBook.generateQuery(query).size() ==0 );
    }

    @Test
    public void booksNotFoundTest() throws NoSuchFieldException, IllegalAccessException {
        String[] query = {"/search", "title", "ethawr"};
        assertTrue("Result is empty list", apiRequestBook.generateQuery(query).size() == 0);
    }

    @Test
    public void queryTestFail() throws NoSuchFieldException, IllegalAccessException {
        String[] query = {"/search", "asd", "Dilan"};
        assertTrue("Result is empty list", apiRequestBook.generateQuery(query).size() == 0);
    }

    @Test
    public void bookTitleTest() {
        book.setTitle("Test");
        assertEquals("Test", book.getTitle());
    }

    @Test
    public void bookGenreTest() {
        List<String> genres = new ArrayList<>();
        genres.add("Test");
        book.setGenre(genres);
        assertEquals(genres, book.getGenre());
    }

    @Test
    public void bookAuthorTest() {
        ArrayList<String> author = new ArrayList<>();
        author.add("Rafif");
        book.setAuthor(author);
        assertEquals(author, book.getAuthor());
    }

    @Test
    public void bookDescriptionTest() {
        book.setDescription("Test");
        assertEquals("Test", book.getDescription());
    }

    @Test
    public void bookImageUrlTest() {
        book.setImageUrl("Test");
        assertEquals("Test", book.getImageUrl());
    }
}
