package com.bot.libby.receiver;

import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RegistrationTest {
    Registration registration;
    User user;

    @Before
    public void setUp(){
        registration = new Registration();
        user = new User("Rafif Taris", "1706979436");
    }

    @Test
    public void testRegisterSuccess(){
        assertTrue(registration.register(new User("seharsas","haadhaasf")));
    }

    @Test
    public void testRegisterFail(){
        registration.register(user);
        assertFalse(registration.register(user));
    }
}
