package com.bot.libby.adapter;

import com.bot.libby.receiver.ApiRequestBook;
import com.bot.libby.resources.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class VolumeToBookListAdapterTest {
    @Mock
    VolumeToBookListAdapter volumeToBookListAdapter;

    @Test
    public void testConvertMethod() {
        String[] message = {"/search", "title", "dilan"};
        ApiRequestBook apiRequestBook = ApiRequestBook.getInstance();
        assertEquals(apiRequestBook.generateQuery(message).getClass().getTypeName(), ArrayList.class.getName());
    }

    @Test
    public void testGetBookList() throws IllegalAccessException, NoSuchFieldException {
        Field field = VolumeToBookListAdapter.class.getDeclaredField("bookList");
        field.setAccessible(true);
        field.set(VolumeToBookListAdapter.getInstance(), new ArrayList<Book>());

        List<Book> result = VolumeToBookListAdapter.getInstance().getBookList();
        assertEquals(result.getClass().getTypeName(), ArrayList.class.getTypeName());
    }
}
