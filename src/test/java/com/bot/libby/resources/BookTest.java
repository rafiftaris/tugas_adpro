package com.bot.libby.resources;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest

public class BookTest {

    @InjectMocks
    private Book book;

    @Test
    public void bookTitleTest(){
        book.setTitle("Test");
        assertEquals("Test", book.getTitle());
    }

    @Test
    public void bookGenreTest(){
        List<String> genres = new ArrayList<>();
        genres.add("Test");
        book.setGenre(genres);
        assertEquals(genres, book.getGenre());
    }

    @Test
    public void bookAuthorTest(){
        ArrayList<String> author = new ArrayList<>();
        author.add("Rafif");
        book.setAuthor(author);
        assertEquals(author, book.getAuthor());
    }
    
    @Test
    public void bookDescriptionTest(){
        book.setDescription("Test");
        assertEquals("Test", book.getDescription());
    }

    @Test
    public void bookImageUrlTest(){
        book.setImageUrl("Test");
        assertEquals("Test", book.getImageUrl());
    }

    @Test
    public void testSetGetIsbn(){
        book.setIsbn("123123");
        assertEquals("123123",book.getIsbn());
    }
}
