package com.bot.libby.resources;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest

public class UserTest {

    @InjectMocks
    private User user;

    @Before
    public void setUp() {
        user = new User("Selvy", "selvyfitriani31");
    }

    @Test
    public void userNameTest(){
        assertEquals("Selvy", user.getName());
    }

    @Test
    public void userIdTest(){
        assertEquals("selvyfitriani31", user.getId());
    }

    @Test
    public void testGetAuthorList(){
        assertEquals(user.getAuthorList().getClass().getTypeName(), AuthorList.class.getTypeName());
    }

    @Test
    public void testSetGetName(){
        user.setName("Rafif");
        assertEquals("Rafif",user.getName());
    }

    @Test
    public void testSetGetId(){
        user.setId("123123");
        assertEquals("123123",user.getId());
    }
}
