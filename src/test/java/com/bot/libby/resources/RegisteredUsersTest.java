package com.bot.libby.resources;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest

public class RegisteredUsersTest {

    @InjectMocks
    private RegisteredUsers registeredUsers;

    @Test
    public void addRegisteredUserTest(){
        User toBeRegisteredUser = new User("Selvy", "selvyfitriani31");
        registeredUsers.addRegisteredUser(toBeRegisteredUser);
        assertEquals("Selvy", registeredUsers.searchUserById("selvyfitriani31").getName());
    }

    @Test
    public void registeredUsersPrintTest(){
        User toBeRegisteredUser = new User ("Selvy", "selvyfitriani31");
        registeredUsers.addRegisteredUser(toBeRegisteredUser);
        assertEquals("Selvy"+"\n", registeredUsers.toString());
    }

    @Test
    public void testSearchUserById(){
        User toBeRegisteredUser = new User ("Selvy", "selvyfitriani31");
        registeredUsers.addRegisteredUser(toBeRegisteredUser);
        assertEquals(registeredUsers.searchUserById(toBeRegisteredUser.getId()),toBeRegisteredUser);
    }

}
