package com.bot.libby.invoker;

import com.bot.libby.commands.AddFavoriteCommand;
import com.bot.libby.commands.ApiRequestCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class LibbyInvokerTest {
    @Spy
    LibbyInvoker libbyInvoker;

    @Test
    public void testInvokerHasCommandList() {
        assertEquals(libbyInvoker.getCommandList().getClass().getTypeName(), ArrayList.class.getTypeName());
    }

    @Test
    public void testAddCommandMethod(){
        libbyInvoker.addCommand(new AddFavoriteCommand());
        assertEquals(1,libbyInvoker.getCommandList().size());
    }

    @Test
    public void testGetCommandList(){
        List commandList = libbyInvoker.getCommandList();
        assertTrue(commandList != null);
    }

    @Test
    public void testExecute(){
        String[] query = {"/search","title","Dilan"};
        ApiRequestCommand apiRequestCommand= new ApiRequestCommand();
        apiRequestCommand.setQuery(query);
        libbyInvoker.addCommand(apiRequestCommand);
        libbyInvoker.execute(0,query);
        apiRequestCommand = (ApiRequestCommand)libbyInvoker.getCommandList().get(0);
        assertTrue(apiRequestCommand.getResult()!=null);
    }
}
