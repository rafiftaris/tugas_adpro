package com.bot.libby.receiver;

import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;

public class ShowList {
    RegisteredUsers registeredUsers = RegisteredUsers.getInstance();

    public String showFav(String userId){
        User user = registeredUsers.searchUserById(userId);
        return user.getFavoriteList().toString();
    }

    public String showSubscribe(String userId){
        User user = registeredUsers.searchUserById(userId);
        return user.getAuthorList().toString();
    }
}
