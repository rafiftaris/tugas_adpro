package com.bot.libby.receiver;

import com.bot.libby.resources.RegisteredUsers;

public class Subscribe {
    private RegisteredUsers registeredUsers = RegisteredUsers.getInstance();

    public Subscribe(){}

    public boolean addAuthor(String newAuthor, String userId) {
        return registeredUsers.searchUserById(userId).getAuthorList().addAuthor(newAuthor);
    }

    public boolean removeAuthor(String author, String userId){
        return registeredUsers.searchUserById(userId).getAuthorList().deleteAuthor(author);
    }
}