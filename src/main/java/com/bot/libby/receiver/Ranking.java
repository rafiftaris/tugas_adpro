package com.bot.libby.receiver;

import com.bot.libby.resources.Book;
import com.bot.libby.resources.MapUtil;
import com.bot.libby.resources.RegisteredUsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ranking {
    private static Ranking ranking = new Ranking();
    private RegisteredUsers registeredUsers = RegisteredUsers.getInstance();
    private Map<Book,Integer> bookCount = new HashMap<>();
    private List<Book> bookRanking;
    private Map<String,Integer> authorCount = new HashMap<>();
    private List<String> authorRanking;

    private Ranking(){}

    public static Ranking getInstance() {
        return ranking;
    }

    public void updateRanking(Book book, String type){
        boolean containsBook = false;
        Book bookKey = null;
        for(Book key : bookCount.keySet()){
            if(key.getIsbn().equals(book.getIsbn())){
                containsBook = true;
                bookKey = key;
                break;
            }
        }

        if(containsBook){
            if(type.equals("add")) bookCount.replace(bookKey,bookCount.get(bookKey)+1);
            else{
                if(bookCount.get(bookKey) <= 1) bookCount.remove(bookKey);
                else bookCount.replace(book,bookCount.get(bookKey)-1);
            }
        }
        else{
            if(type.equals("add")) bookCount.put(book,1);
            else System.err.println(1);
        }
        bookCount = MapUtil.sortByValue(bookCount);

    }

    public void updateRanking(String author, String type){
        if(authorCount.containsKey(author)){
            if(type.equals("add"))authorCount.replace(author,authorCount.get(author)+1);
            else{
                if(authorCount.get(author) <= 1) authorCount.remove(author);
                else authorCount.replace(author,authorCount.get(author)-1);
            }
        }
        else{
            if(type.equals("add"))authorCount.put(author,1);
            else System.err.println(1);
        }
        authorCount = MapUtil.sortByValue(authorCount);

    }

    public List<Book> showBookRanking(){
        bookRanking = new ArrayList<>();
        for (Map.Entry<Book, Integer> entry  : bookCount.entrySet()){
            bookRanking.add(entry.getKey());
        }
        return bookRanking;
    }

    public List<String> showAuthorRanking(){
        authorRanking = new ArrayList<>();
        for (Map.Entry<String, Integer> entry  : authorCount.entrySet()){
            authorRanking.add(entry.getKey());
        }
        return authorRanking;
    }

    public RegisteredUsers getRegisteredUsers() {
        return registeredUsers;
    }
}
