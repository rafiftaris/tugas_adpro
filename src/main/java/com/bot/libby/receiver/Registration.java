package com.bot.libby.receiver;

import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;

public class Registration {
    private RegisteredUsers registeredUsers;

    public Registration(){
        this.registeredUsers = RegisteredUsers.getInstance();
    }

    public boolean register(User user){
        if(registeredUsers.searchUserById(user.getId()) == null){
            registeredUsers.addRegisteredUser(user);
            return true;
        }
        return false;
    }

}
