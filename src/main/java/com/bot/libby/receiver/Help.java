package com.bot.libby.receiver;

import com.bot.libby.invoker.LibbyInvoker;

public class Help {
    private LibbyInvoker libbyInvoker;
    private StringBuilder result;

    public Help(LibbyInvoker libbyInvoker){this.libbyInvoker = libbyInvoker;}

    public String showHelp(){
        result = new StringBuilder();
        result.append("LIST COMMAND\n"+
                "==============================\n");
        for(int i=0;i<libbyInvoker.getCommandList().size();i++){
            result.append(libbyInvoker.getCommandList().get(i).toString());
            if(i<libbyInvoker.getCommandList().size()-1) result.append("\n\n");
        }
        return result.toString();
    }
}
