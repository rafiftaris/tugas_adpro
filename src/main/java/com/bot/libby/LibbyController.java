package com.bot.libby;

import com.bot.libby.commands.*;
import com.bot.libby.invoker.LibbyInvoker;
import com.bot.libby.receiver.*;

public class LibbyController {
    private LibbyInvoker libbyInvoker;
    private ApiRequestCommand apiRequestCommand;
    private ApiRequestBook apiRequestBook;
    private RankingCommand rankingCommand;
    private Ranking ranking;
    private RegistrationCommand registrationCommand;
    private Registration registration;
    private AddFavoriteCommand addFavoriteCommand;
    private RemoveFavoriteCommand removeFavoriteCommand;
    private Favorite favorite;
    private SubscribeAuthorCommand subscribeAuthorCommand;
    private UnsubscribeAuthorCommand unsubscribeAuthorCommand;
    private Subscribe subscribe;
    private ShowFavCommand showFavCommand;
    private ShowSubscribeCommand showSubscribeCommand;
    private ShowList showList;
    private Help help;
    private HelpCommand helpCommand;

    public LibbyController() {
        libbyInvoker = new LibbyInvoker();
        // TODO create receivers
        createReceivers();
        createCommands();
        setCommands();
    }

    public LibbyInvoker getLibbyInvoker() {
        return this.libbyInvoker;
    }

    public void setLibbyInvoker(LibbyInvoker libbyInvoker) {
        this.libbyInvoker = libbyInvoker;
    }

    public void createReceivers() {
        apiRequestBook = ApiRequestBook.getInstance();
        ranking = Ranking.getInstance();
        registration = new Registration();
        subscribe = new Subscribe();
        favorite = new Favorite();
        showList = new ShowList();
        help = new Help(libbyInvoker);
        //TODO Receiver lainnya
    }

    public void createCommands() {
        apiRequestCommand = new ApiRequestCommand();
        rankingCommand = new RankingCommand();
        registrationCommand = new RegistrationCommand(registration);
        addFavoriteCommand = new AddFavoriteCommand();
        removeFavoriteCommand = new RemoveFavoriteCommand();
        subscribeAuthorCommand = new SubscribeAuthorCommand(subscribe);
        unsubscribeAuthorCommand = new UnsubscribeAuthorCommand(subscribe);
        showFavCommand = new ShowFavCommand(showList);
        showSubscribeCommand = new ShowSubscribeCommand(showList);
        helpCommand = new HelpCommand(help);
        //TODO Command lainnya
    }

    public void setCommands() {
        libbyInvoker.addCommand(apiRequestCommand);
        libbyInvoker.addCommand(rankingCommand);
        libbyInvoker.addCommand(registrationCommand);
        libbyInvoker.addCommand(addFavoriteCommand);
        libbyInvoker.addCommand(removeFavoriteCommand);
        libbyInvoker.addCommand(subscribeAuthorCommand);
        libbyInvoker.addCommand(unsubscribeAuthorCommand);
        libbyInvoker.addCommand(showFavCommand);
        libbyInvoker.addCommand(showSubscribeCommand);
        libbyInvoker.addCommand(helpCommand);
        //TODO Set command lainnya
    }

    public void executeCommand(int index, String[] parameters) {
        libbyInvoker.execute(index, parameters);
    }
}
