package com.bot.libby.commands;
import com.bot.libby.receiver.Favorite;

public class RemoveFavoriteCommand implements Command{
    private Favorite favorite;
    private String[] query;
    public boolean success;

    public RemoveFavoriteCommand(){
        favorite = new Favorite();
    }

    @Override
    public void execute(){
        //TODO : Implement this!
        success = this.favorite.removeBook(query[1],query[2]);
    }

    @Override
    public void setQuery(String[] query) {
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/unfav [isbn]\n" +
                "Menghilangkan buku dari list favorit");
        return help.toString();
    }
}
