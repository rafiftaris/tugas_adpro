package com.bot.libby.commands;

import com.bot.libby.receiver.ShowList;

public class ShowFavCommand implements Command {
    private String[] query;
    private ShowList showList;
    private String result;

    public ShowFavCommand(ShowList showList){this.showList = showList;}

    @Override
    public void execute() {
        result = showList.showFav(query[1]);
    }

    public void setQuery(String[] query){
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }

    public String getResult(){
        return this.result;
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/showfav\n" +
                "Melihat list favorit");
        return help.toString();
    }
}
