# LibbyBOT

## Project Source Code Home
This is the main code of the LibbyBot Chatbot program. Each folder consists of a single
app that may be further developed in the future. Tests for each Class can be 
found in Test folder with similar directories to the source codes.

## Contributors

1. [**Muhammad Rezki**](https://gitlab.com/muhammad.rezki/)
2. [**Moh. Faisal**](https://gitlab.com/mohfaisal25/)
3. [**Rafif Taris**](https://gitlab.com/rafiftaris/)
4. [**Selvy Fitriani**](https://gitlab.com/selvyfitriani31/)
5. [**Valerysa Regita**](https://gitlab.com/valerysazz/)

## App description
This app contains adapter classes needed by the LibbyBot program.

## Project Task Checklist

- [ ] Week 2: -


## My Notes - Project Source Code Home

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1.