package com.bot.libby.adapter;

import com.google.api.services.books.model.Volumes;

public interface Adapter {

    public void convertVolumes(Volumes volumes);
}
