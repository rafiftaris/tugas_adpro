package com.bot.libby.invoker;

import com.bot.libby.commands.Command;

import java.util.ArrayList;
import java.util.List;

public class LibbyInvoker {
    private List<Command> commandList= new ArrayList<>();

    public LibbyInvoker(){}

    public void addCommand(Command command){
        commandList.add(command);
    }

    public List getCommandList(){
        return commandList;
    }

    public void execute(int index, String[] parameters){
        commandList.get(index).setQuery(parameters);
        commandList.get(index).execute();
    }
}
