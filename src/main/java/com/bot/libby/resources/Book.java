package com.bot.libby.resources;

import java.util.List;

public class Book {
    private String title;
    private List<String> genre;
    private List<String> author;
    private String description;
    private String imageUrl;
    private String isbn;

    public Book(String title, List<String> genre, List<String> author, String description, String imageUrl, String isbn){
        this.title = title;
        this.genre = genre;
        this.author = author;
        this.description = description;
        this.imageUrl = imageUrl;
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public List<String> getAuthor() {
        return author;
    }

    public void setAuthor(List<String> author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.title + "\n");

        if(isbn != null) result.append("ISBN : " + this.isbn + "\n");

        if(author != null){
            result.append("Author : ");
            for(int j=0;j<this.author.size();j++){
                result.append(this.author.get(j));
                if(j<this.author.size()-1) result.append(", ");
            }
            result.append("\n");
        }

        if(genre!=null){
            result.append("Genre : ");
            for(int k=0;k<this.genre.size();k++){
                result.append(this.genre.get(k));
                if(k<this.genre.size()-1) result.append(", ");
            }
            result.append("\n");
        }

        return result.toString();
    }
}
