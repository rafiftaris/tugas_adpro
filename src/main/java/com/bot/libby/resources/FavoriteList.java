package com.bot.libby.resources;

import com.bot.libby.receiver.Ranking;

import java.util.ArrayList;

public class FavoriteList {

    private ArrayList<Book> favoriteList;
    private Ranking ranking = Ranking.getInstance();

    public FavoriteList() {
        this.favoriteList = new ArrayList<Book>();
    }

    public boolean addBook(Book book){
        if (isContains(book)) {
            return false;
        } else {
            favoriteList.add(book);
            ranking.updateRanking(book, "add");
            return true;
        }
    }

    public boolean deleteBook(Book book){
        if (isContains(book)) {
            favoriteList.remove(search(book));
            ranking.updateRanking(book, "remove");
            return true;
        } else {
            return false;
        }
    }

    public Book search(Book book){
        for(int i = 0;i<favoriteList.size();i++){
            if(favoriteList.get(i).getIsbn().equals(book.getIsbn())) return favoriteList.get(i);
        }
        return null;
    }

    // Pendekin nama method
    public ArrayList<Book> getFavoriteList() {
        return this.favoriteList;
    }

    public boolean isContains(Book book) {
        for(int i = 0;i<favoriteList.size();i++){
            if(favoriteList.get(i).getIsbn().equals(book.getIsbn())) return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("DAFTAR BUKU FAVORIT\n" +
                "========================================\n");
        for(int i=0;i<favoriteList.size();i++){
            result.append(i+1 + ". ");
            result.append(favoriteList.get(i).toString());
            if(i<favoriteList.size()-1) result.append("\n\n");
        }
        return result.toString();
    }

}
