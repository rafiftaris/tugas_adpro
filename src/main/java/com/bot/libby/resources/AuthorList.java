package com.bot.libby.resources;

import com.bot.libby.receiver.Ranking;

import java.util.ArrayList;

public class AuthorList {

    private ArrayList<String> authorList;
    private Ranking ranking = Ranking.getInstance();

    public AuthorList (){
        this.authorList = new ArrayList<>();
    }

    public boolean addAuthor(String name){
        if (isContains(name)) {
            return false;
        } else {
            authorList.add(name);
            ranking.updateRanking(name, "add");
            return true;
        }
    }

    public boolean deleteAuthor(String name){
        if (isContains(name)) {
            authorList.remove(name);
            ranking.updateRanking(name, "delete");
            return true;
        } else {
            return false;
        }
    }

    public boolean isContains(String name) {
        return authorList.contains(name);
    }

    public ArrayList<String> getList() {
        return this.authorList;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("DAFTAR SUBSCRIBE\n" +
                "========================================\n");
        for(int i=0;i<authorList.size();i++){
            result.append(i+1 + ". " + authorList.get(i) + "\n");
        }
        return result.toString();
    }


}
